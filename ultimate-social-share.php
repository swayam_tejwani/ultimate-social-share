<?php
/*
Plugin Name: Ultimate Social Share
Description: Plugin to show social share icons on your website.
Version: 1.0
Author: Swayam Tejwani
Text Domain: ultimate-social-share
Author URI: profiles.wordpress.org/swayamtejwani
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*
* Create page in worpdress admin.
*/
function uss_admin_menu() {
	$uss_admin_suffix = add_menu_page( 'Ultimate Social Share', 'Ultimate Social Share', 'manage_options', 'ultimate-social-share', 'uss_admin_page' );
}
add_action( 'admin_menu', 'uss_admin_menu' );

/*
* Include file to render admin page.
*/
function uss_admin_page(){
	include ('admin/uss-admin-page.php');
}

/*
* Include assets for admin page.
*/
function uss_load_admin_page_assets( $hook ){
	
	if( $hook != 'toplevel_page_ultimate-social-share' ){
		return;
	}
	
	wp_enqueue_script( 'uss-jqueryui-script', plugins_url('admin/js/jquery-ui.js', __FILE__), array('jquery') );

	wp_enqueue_style( 'uss-jqueryui-style', plugins_url('admin/css/jquery-ui.css', __FILE__) );

	wp_enqueue_style( 'uss-admin-css', plugins_url('admin/css/admin-style.css', __FILE__) );

	wp_enqueue_style( 'wp-color-picker' );

	wp_enqueue_script( 'uss-admin-script', plugins_url('admin/js/admin-script.js', __FILE__), array('jquery','uss-jqueryui-script','wp-color-picker') );

	wp_localize_script( 'uss-admin-script', 'uss_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

	wp_enqueue_style( 'uss-load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );

}
add_action( 'admin_enqueue_scripts', 'uss_load_admin_page_assets' );


function uss_load_front_assets(){

	wp_enqueue_style( 'uss-load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );

	wp_enqueue_style( 'uss-front-css', plugins_url('css/uss-style.css',__FILE__) );
}
add_action( 'wp_enqueue_scripts', 'uss_load_front_assets' );

/*
* Ajax Call back to save admin settings.
*/
function uss_update_settings(){

	if(isset($_POST)){
		$selected_pt = $_POST['selected_pt'];
		$selected_icons = $_POST['selected_icons'];
		$icon_size = $_POST['icon_size'];
		$icon_color = $_POST['icon_color'];
		$icons_placement = $_POST['icons_placement'];

		$uss_options = array(
							'selected_pt'=> $selected_pt,
							'selected_icons'=> $selected_icons,
							'icon_size'	=> $icon_size,
							'icon_color'=> $icon_color,
							'icons_placement'=>$icons_placement
						);
		delete_option('uss_options');
		$is_updated = update_option('uss_options',$uss_options);

		if($is_updated){
			echo "UPDATED";
		}
	}
	die;
}
add_action( 'wp_ajax_uss_update_settings','uss_update_settings' );

/*
* Get Social Share options.
*/
function uss_get_options(){
	$uss_options = get_option('uss_options');
	return $uss_options;
}


/*
* Display Social icons below post content.
*/
function uss_add_social_icons_content( $content ){
	$uss_options = uss_get_options();
	$str = '';
	$size_class = '';
	if(!empty($uss_options)){
		$current_pt = get_post_type();
		$allowed_pt = $uss_options['selected_pt'];
		$allowed_pt = explode(",", $allowed_pt);
		$placement = $uss_options['icons_placement'];
		$placement = explode(",", $placement);

		if(in_array($current_pt, $allowed_pt) && is_singular($allowed_pt) && (in_array('after_content', $placement) || in_array('below_title', $placement))){

			$icon_color = $uss_options['icon_color'];
			if(!empty($icon_color)){
				$icon_color = 'color:'.$icon_color.';';
			}else{
				$icon_color = '';
			}

			 $str .= '<ul class="uss-icons">';

			 if($uss_options['icon_size'] == "small"){
			 	$size_class = 'fa-lg';
			 }else if($uss_options['icon_size'] == "medium"){
			 	$size_class = 'fa-2x';
			 }else if($uss_options['icon_size'] == "large"){
			 	$size_class = 'fa-3x';
			 }

			 $social_options = $uss_options['selected_icons'];
			 $social_options = explode(',', $social_options);
			 foreach( $social_options as $icon){
			 	if($icon == "facebook"){
			 		$str .= '<li><a href="http://www.facebook.com/sharer.php?u='.get_permalink().'" target="_blank" title="'.__('Share on Facebook','ultimate-social-share').'"><i class="fa fa-facebook '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "twitter"){
		 			$str .= '<li><a href="http://twitter.com/intent/tweet?text='.the_title_attribute(array('echo'=>false)).'&url='.get_permalink().'" target="_blank" title="'.__('Share on Twitter','ultimate-social-share').'"><i class="fa fa-twitter '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "google+"){
			 		$str .= '<li><a href="https://plus.google.com/share?url='.get_permalink().'" target="_blank" title="'.__('Share on Google Plus','ultimate-social-share').'"><i class="fa fa-google-plus '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "pinterest"){
			 		$str .= '<li><a href="https://pinterest.com/pin/create/button/?url='.get_permalink().'" target="_blank" title="'.__('Share on Pinterest','ultimate-social-share').'"><i class="fa fa-pinterest-p '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "linkedin"){
			 		$str .= '<li><a href="https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink().'&title='.the_title_attribute(array('echo'=>false)).'" target="_blank"><i class="fa fa-linkedin '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "whatsapp" && wp_is_mobile() == true){
			 		$str .= '<li><a href="whatsapp://send?text='.get_permalink().'" data-action="share/whatsapp/share"><i class="fa fa-whatsapp '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}
			 }
			 $str .= '</ul>';
		}
		if(in_array('below_title', $placement)){
			$content = $str.''.$content;
		}

		if(in_array('after_content', $placement)){
			$content = $content.''.$str;
		}
	}
	return $content;
}
add_filter('the_content','uss_add_social_icons_content');

/*
Shortcode [uss_display_icons] call back function to display social icons.
*/
function uss_display_icons_callbk(){
	ob_start();
	$uss_options = uss_get_options();
	$str = '';
	$size_class = '';
	if(!empty($uss_options)){
		$current_pt = get_post_type();
		$allowed_pt = $uss_options['selected_pt'];
		$allowed_pt = explode(",", $allowed_pt);
		$placement = $uss_options['icons_placement'];
		$placement = explode(",", $placement);

		if(in_array($current_pt, $allowed_pt) && is_singular($allowed_pt) && (in_array('after_content', $placement) || in_array('below_title', $placement))){

			$icon_color = $uss_options['icon_color'];
			if(!empty($icon_color)){
				$icon_color = 'color:'.$icon_color.';';
			}else{
				$icon_color = '';
			}

			 $str .= '<ul class="uss-icons">';

			 if($uss_options['icon_size'] == "small"){
			 	$size_class = 'fa-lg';
			 }else if($uss_options['icon_size'] == "medium"){
			 	$size_class = 'fa-2x';
			 }else if($uss_options['icon_size'] == "large"){
			 	$size_class = 'fa-3x';
			 }

			 $social_options = $uss_options['selected_icons'];
			 $social_options = explode(',', $social_options);
			 foreach( $social_options as $icon){
			 	if($icon == "facebook"){
			 		$str .= '<li><a href="http://www.facebook.com/sharer.php?u='.get_permalink().'" target="_blank" title="'.__('Share on Facebook','ultimate-social-share').'"><i class="fa fa-facebook '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "twitter"){
		 			$str .= '<li><a href="http://twitter.com/intent/tweet?text='.the_title_attribute(array('echo'=>false)).'&url='.get_permalink().'" target="_blank" title="'.__('Share on Twitter','ultimate-social-share').'"><i class="fa fa-twitter '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "google+"){
			 		$str .= '<li><a href="https://plus.google.com/share?url='.get_permalink().'" target="_blank" title="'.__('Share on Google Plus','ultimate-social-share').'"><i class="fa fa-google-plus '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "pinterest"){
			 		$str .= '<li><a href="https://pinterest.com/pin/create/button/?url='.get_permalink().'" target="_blank" title="'.__('Share on Pinterest','ultimate-social-share').'"><i class="fa fa-pinterest-p '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "linkedin"){
			 		$str .= '<li><a href="https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink().'&title='.the_title_attribute(array('echo'=>false)).'" target="_blank"><i class="fa fa-linkedin '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "whatsapp" && wp_is_mobile() == true){
			 		$str .= '<li><a href="whatsapp://send?text='.get_permalink().'" data-action="share/whatsapp/share"><i class="fa fa-whatsapp '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}
			 }
			 $str .= '</ul>';
		}
	}
	echo $str;
	return ob_get_clean();
}
add_shortcode('uss_display_icons','uss_display_icons_callbk');


function uss_left_floated_icons(){
$uss_options = uss_get_options();
	$str = '';
	$size_class = '';
	if(!empty($uss_options)){
		$current_pt = get_post_type();
		$allowed_pt = $uss_options['selected_pt'];
		$allowed_pt = explode(",", $allowed_pt);
		$placement = $uss_options['icons_placement'];
		$placement = explode(",", $placement);

		if(in_array($current_pt, $allowed_pt) && is_singular($allowed_pt) && in_array('left_floated', $placement)){

			$icon_color = $uss_options['icon_color'];
			if(!empty($icon_color)){
				$icon_color = 'color:'.$icon_color.';';
			}else{
				$icon_color = '';
			}

			 $str .= '<ul class="social_side_links">';

			 if($uss_options['icon_size'] == "small"){
			 	$size_class = 'fa-lg';
			 }else if($uss_options['icon_size'] == "medium"){
			 	$size_class = 'fa-2x';
			 }else if($uss_options['icon_size'] == "large"){
			 	$size_class = 'fa-3x';
			 }

			 $social_options = $uss_options['selected_icons'];
			 $social_options = explode(',', $social_options);
			 foreach( $social_options as $icon){
			 	if($icon == "facebook"){
			 		$str .= '<li><a href="http://www.facebook.com/sharer.php?u='.get_permalink().'" target="_blank" title="'.__('Share on Facebook','ultimate-social-share').'"><i class="fa fa-facebook '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "twitter"){
		 			$str .= '<li><a href="http://twitter.com/intent/tweet?text='.the_title_attribute(array('echo'=>false)).'&url='.get_permalink().'" target="_blank" title="'.__('Share on Twitter','ultimate-social-share').'"><i class="fa fa-twitter '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "google+"){
			 		$str .= '<li><a href="https://plus.google.com/share?url='.get_permalink().'" target="_blank" title="'.__('Share on Google Plus','ultimate-social-share').'"><i class="fa fa-google-plus '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "pinterest"){
			 		$str .= '<li><a href="https://pinterest.com/pin/create/button/?url='.get_permalink().'" target="_blank" title="'.__('Share on Pinterest','ultimate-social-share').'"><i class="fa fa-pinterest-p '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "linkedin"){
			 		$str .= '<li><a href="https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink().'&title='.the_title_attribute(array('echo'=>false)).'" target="_blank"><i class="fa fa-linkedin '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}else if($icon == "whatsapp" && wp_is_mobile() == true){
			 		$str .= '<li><a href="whatsapp://send?text='.get_permalink().'" data-action="share/whatsapp/share"><i class="fa fa-whatsapp '.$size_class.'" aria-hidden="true" style="'.$icon_color.'"></i></a></li>';
			 	}
			 }
			 $str .= '</ul>';
		}
		echo $str;
	}
}
add_action('wp_footer','uss_left_floated_icons');