jQuery(function () {
    //jQuery('.portlet-content').css({'display':'none'});
    
    var sortedIds = jQuery(".column").sortable({
      cursor: "move"
    });
    
    jQuery(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<span class='ui-icon ui-icon-minusthick'></span>")
        .end()
        .find(".portlet-content");
    
    jQuery(".portlet-header .ui-icon").click(function () {
        jQuery(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
        jQuery(this).parents(".portlet:first").find(".portlet-content").toggle();
    });
    jQuery(".column").disableSelection();


    var radios = jQuery('#sizes_group input:radio');
    if(radios.is(':checked') === false) {
        radios.filter('[value=small]').prop('checked', true);
        jQuery('.icons-demo .fa-lg').show();
        radios.filter('[value=small]').attr('checked', true);
        //radios.attr('checked', 'checked');
    }

    jQuery('#sizes_group input:radio').change(function(){
        var icon_size = jQuery(this).val();
        jQuery('.icons-demo i').hide();
        jQuery('.'+icon_size).show();
    });

    jQuery('.icon-color-picker').wpColorPicker();

    jQuery('#uss_form').submit(function(e){
        e.preventDefault();
        
        var selected_pt = [];
        jQuery('#uss_post_types input[type="checkbox"]:checked').each(function() {
            selected_pt.push(jQuery(this).attr('value'));
        });
        
        if(selected_pt.length === 0){
            alert("Please Select Post Type");
            return false;
        }

        var selected_icons = [];
        jQuery('.column input[type="checkbox"]:checked').each(function() {
            selected_icons.push(jQuery(this).attr('value'));
        });

        if(selected_icons.length === 0){
            alert("Please enable atleast one social network");
            return false;
        }

        var icons_placement = [];
        jQuery('#icons_placement_cb input[type="checkbox"]:checked').each(function() {
            icons_placement.push(jQuery(this).attr('value'));
        });

        if(icons_placement.length === 0){
            alert("Please select icon placement");
            return false;
        }

        var icon_color = jQuery('#icon_color').val();
        var regEx = /^#[a-f0-9]{6}$/i;
        if(icon_color == ""){

        }else if(!regEx.test(icon_color)){
            alert("Invalid Color Selection");
            return false;
        }
        jQuery('.loading-img').show();
        jQuery('.div-response').hide();
        jQuery.ajax({
            url:uss_ajax_object.ajax_url,
            type:'POST',
            data:{
                action: 'uss_update_settings',
                'selected_pt': selected_pt.toString(),
                'selected_icons': selected_icons.toString(),
                'icon_size': jQuery('#sizes_group input[type="radio"]:checked').val(),
                'icon_color': jQuery('#icon_color').val(),
                'icons_placement': icons_placement.toString()
            },
            success: function(response){
                jQuery('.loading-img').hide();
                if(response == "UPDATED"){
                    jQuery('.div-response').show();
                }
            }
        });

    })

});