<div class="wrap">
	<h1><?php _e('Ultimate Social Share Settings','ultimate-social-share'); ?></h1>
    <?php
    //$uss_options = uss_get_options();
    $uss_options = get_option('uss_options');
    ?>
	<form method="post" action="" id="uss_form">
		<table class="form-table">
            <tr valign="top">
                <th scope="row">
                	<label for="pt_cb">
                		<?php _e('Enable/Disable post types to display social icons','ultimate-social-share'); ?>
        			</label> 
                </th>
                <td>
            		<?php
                        if(!empty($uss_options['selected_pt'])){
                            $selected_pts = explode(',',$uss_options['selected_pt']); 
                        }else{
                            $selected_pts = array();
                        }
                		$args = array( 'public' => true );
                		$output = 'names';
                		$post_types = get_post_types( $args, $output );
                		echo "<div id='uss_post_types'>";
                		foreach ( $post_types as $post_type ) {
                            if(in_array($post_type, $selected_pts)){
                                $checked = 'checked';
                            }else{
                                $checked = '';
                            }
					   		?>
					   		<label>
					   		<input type="checkbox" name="pt_cb[]" value="<?php echo $post_type; ?>" <?php echo $checked; ?>><?php echo ucfirst($post_type); ?>
					   		</label>
							<?php	
						}
						echo '</div>';
            		?>
                </td>
            </tr>

            <tr valign="top">
            	<th scope="row">
            		<label><?php _e('Set order of social icons with Enable/Disable','ultimate-social-share'); ?></label>
            	</th>	
            	<td>
            		<div class="column">
            			<?php
                            if(!empty($uss_options['selected_icons'])){
                                $selected_icons = explode(',',$uss_options['selected_icons']); 
                            }else{
                                $selected_icons = array();
                            }
            				$social_icons = array(
                						'facebook' => 'Facebook',
                						'twitter' => 'Twitter',
                						'google+' => 'Google+',
                						'pinterest' => 'Pinterest',
                						'linkedin' => 'Linkedin',
                						'whatsapp' => 'Whatsapp ( For Mobile Browsers )'
            							);
        				foreach ($social_icons as $social_key => $social_value) {
                            if(in_array($social_key, $selected_icons)){
                                $checked = 'checked';
                            }else{
                                $checked = '';
                            }
        					?>
        					<div class="portlet" id="<?php echo $social_key; ?>">
    						<div class="portlet-header"><?php echo $social_value; ?></div>
    						<div class="portlet-content"><label><?php _e('Enable ','ultimate-social-share'); ?><input type="checkbox" name="uss_icons[]" value="<?php echo $social_key; ?>" <?php echo $checked; ?>></label></div>
        					</div>
        					<?php
        				}
            			?>
					</div>
            	</td>
            </tr>

            <tr valign="top">
            	<th><label><?php _e('Select social icon size','ultimate-social-share'); ?></label></th>
            	<td id="sizes_group">
                    <?php
                    if(!empty($uss_options['icon_size'])){
                        $icon_size = $uss_options['icon_size'];
                    }else{
                        $icon_size = '';
                    }
                    ?>
            		<label><input type="radio" name="uss_icons_size" value="small" <?php checked($icon_size,'small'); ?>><?php _e('Small','ultimate-social-share'); ?></label>
            		<label><input type="radio" name="uss_icons_size" value="medium" <?php checked($icon_size,'medium'); ?>><?php _e('Medium','ultimate-social-share'); ?></label>
            		<label><input type="radio" name="uss_icons_size" value="large" <?php checked($icon_size,'large'); ?>><?php _e('Large','ultimate-social-share'); ?></label>
            	</td>
            </tr>
            <tr valign="top" class="icons-demo-container">
            	<th></th>
            	<td>
            		<div class="icons-demo">
		            	<i class="fa fa-facebook fa-lg small" aria-hidden="true" <?php if($icon_size == "small"){echo 'style="display:block;"';}?>></i>
		            	<i class="fa fa-facebook fa-2x medium" aria-hidden="true" <?php if($icon_size == "medium"){echo 'style="display:block;"';}?>></i>
		            	<i class="fa fa-facebook fa-3x large" aria-hidden="true" <?php if($icon_size == "large"){echo 'style="display:block;"';}?>></i>
		            </div>
	            </td>
    		</tr>
    		<tr valign="top">
    			<th>
    				<label><?php _e('Select icon color','ultimate-social-share'); ?></label>
    			</th>
    			<td>
                    <?php
                    if(!empty($uss_options['icon_color'])){
                        $icon_color = $uss_options['icon_color'];
                    }else{
                        $icon_color = '';
                    }
                    ?>
    				<input type="text" name="icon_color" id="icon_color" class="icon-color-picker" value="<?php echo $icon_color; ?>">
    			</td>
    		</tr>
    		<tr valign="top">
    			<th>
					<label><?php _e('Select social icons placement','ultimate-social-share'); ?></label>
    			</th>
    			<td id="icons_placement_cb">
                    <?php
                    if(!empty($uss_options['icons_placement'])){
                        $icons_placement = $uss_options['icons_placement'];
                        $icons_placement = explode(',', $icons_placement);
                    }else{
                        $icons_placement = '';
                    }
                    
                    ?>
    				<label><input type="checkbox" name="icons_placement[]" value="below_title" <?php if(!empty($icons_placement)){ if(in_array('below_title', $icons_placement)){echo "checked";} } ?>><?php _e('Below Post Title','ultimate-social-share'); ?></label>
    				<label><input type="checkbox" name="icons_placement[]" value="left_floated" <?php if(!empty($icons_placement)){ if(in_array('left_floated', $icons_placement)){echo "checked";} } ?>><?php _e('Floating on the left area','ultimate-social-share'); ?></label>
    				<label><input type="checkbox" name="icons_placement[]" value="after_content" <?php if(!empty($icons_placement)){ if(in_array('after_content', $icons_placement)){echo "checked";} } ?>><?php _e('After Post Content','ultimate-social-share'); ?></label>
    			</td>
    		</tr>
            <tr valign="top">
            	<th>
            		<input type="submit" class="button button-primary" name="uss_submit" value="Submit">
            		<img src="<?php echo plugins_url('css/images/ajax-loader.gif',__FILE__); ?>" class="loading-img">
            	</th>
            	<td></td>
            </tr>
        </table>
        <div class="div-response">
			<p><strong><?php _e('Settings saved.','ultimate-social-share'); ?></strong></p>
		</div>
	</form>
</div>